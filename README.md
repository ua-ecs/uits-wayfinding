Wayfinding for the Computer Center at the University of Arizona
===============================================================

We get a lot of visitors to the Computer Center at the U of A.  The idea behind this project is 
to put an Echo device at the reception area when you first enter the building.  Visitors could
then ask for directions to specific rooms, or to look up people in the directory and be directed
to their room.

This project was created in a huge rush for the 2016 re:Invent Alexa challenge. 

https://www.hackster.io/contests/alexa-reinvent

https://www.hackster.io/mark-fischer/uits-wayfinding-bf199a

